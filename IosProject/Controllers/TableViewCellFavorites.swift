//
//  TableViewCellFavorites.swift
//  IosProject
//
//  Created by Dror Manzur on 01/02/2018.
//  Copyright © 2018 Adi Mashiah. All rights reserved.
//

import UIKit

class TableViewCellFavorites: UITableViewCell {

    
    @IBOutlet weak var placeImageView: UIImageView!    
    @IBOutlet weak var placeDescriptionLabel: UILabel!
    @IBOutlet weak var placeNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
