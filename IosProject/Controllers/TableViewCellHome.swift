//
//  TableViewCellHome.swift
//  IosProject
//
//  Created by Adi Mashiah on 19/12/2017.
//  Copyright © 2017 Adi Mashiah. All rights reserved.
//

import UIKit

class TableViewCellHome: UITableViewCell {
    
    @IBOutlet weak var placeNameLabel: UILabel!
    
    @IBOutlet weak var PublisherNameLabel: UILabel!
    @IBOutlet weak var placePictureImageView: UIImageView!
    @IBOutlet weak var placeDescriptionLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
