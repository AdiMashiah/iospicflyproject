//
//  ViewControllerImportImage.swift
//  IosProject
//
//  Created by Adi Mashiah on 19/12/2017.
//  Copyright © 2017 Adi Mashiah. All rights reserved.
//

import UIKit
import WebKit
import SwiftSoup

class ViewControllerImportImage: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIWebViewDelegate {

    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var galleryButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var imageViewImport: UIImageView!
    

    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    var place:Place?
    var imageToSearch:UIImage?
    var imageURL:String?
    let webView = UIWebView()
    
    

    
    @IBAction func searchImage(_ sender: UIButton) {
        if (imageToSearch != nil){
            activityIndicator.startAnimating()
            self.searchButton.isEnabled = false
            Model.instance.searchImage(image: imageToSearch!)
        }
    }
    
    
    @IBAction func openCamera(_ sender: UIButton) {
        
         if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            let picker=UIImagePickerController()
            picker.delegate = self
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.allowsEditing=true
            self.present(picker,animated:true,completion:nil)
        }
        
    }
    
    
    @IBAction func openGallery(_ sender: UIButton) {
       

        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)){
            let picker=UIImagePickerController()
            picker.delegate = self
            picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            picker.allowsEditing=true
            self.present(picker,animated:true,completion:nil)
        }

    }
    

     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let picker = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.imageViewImport.image=picker
            self.imageToSearch = picker
            self.searchButton.isEnabled = true
        }
        else{
            print("error in ")
        }
        dismiss(animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator.frame = CGRect(x:0,y:0,width:100,height:100)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        activityIndicator.center = self.view.center
        
        view.addSubview(activityIndicator)
        
        self.searchButton.isEnabled = false
        
        ModelNotification.GetInfoFromGoogle.observe(callback: { (value) in
            if value! != nil {
                ModelNotification.removeObserver(observer: self)
                self.extract(imageURL:value!!)
            }
            else {
                self.activityIndicator.stopAnimating()
                print("not recived data from google in import view controller")
               self.place = nil
            }
        })
    }
    
    func extract(imageURL:String){
        print(imageURL)
        let escapedString = imageURL.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let ns = escapedString!.replacingOccurrences(of: "=", with: "%3D").replacingOccurrences(of: "&", with: "%26")
        guard let url = URL(string: "https://www.google.com/searchbyimage?&image_url=" + ns) else { return }

        self.webView.delegate = self
        
        
       // self.webView.frame = CGRect(x: 0, y: 0, width: 300, height: 500)  //no need 4 that
        
        let request = URLRequest(url:url)
        self.webView.loadRequest(request)
        
        //view.addSubview(self.webView) //no need 4 that
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.activityIndicator.stopAnimating()
         self.place = nil
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.activityIndicator.stopAnimating()

        let htmlString = self.webView.stringByEvaluatingJavaScript(from: "document.getElementsByTagName('html')[0].innerHTML")
        do {
            let doc =  try SwiftSoup.parse(htmlString!)
            print(doc)
            let placeName = try doc.getElementsByClass("_hUb").text()
            let placeDetails = try doc.getElementsByClass("kp-body").tagName("span").first()?.text()
            if doc != nil && placeName != nil  {
                 let name = String(placeName.dropFirst(11))
                print("place was found!!!")
                if placeDetails != nil {
                    self.place = Place(name: name,description: placeDetails!,image:self.imageToSearch!)
                }
                else{
                    self.place = Place(name: name,description: "no details about this place",image:self.imageToSearch!)
                }
            
                self.performSegue(withIdentifier: "placeDetailsSegue", sender: self)
            }
        } catch {
            self.place = nil
        }
    
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is ViewControllerPlaceDetails
        {
            let vc = segue.destination as? ViewControllerPlaceDetails
            vc?.placeDetails = self.place!
            vc?.vcImportImage = true

        }
    }



}
