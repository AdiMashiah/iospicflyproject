//
//  ViewControllerLogout.swift
//  IosProject
//
//  Created by Adi Mashiah on 19/12/2017.
//  Copyright © 2017 Adi Mashiah. All rights reserved.
//

import UIKit

class ViewControllerLogout: UIViewController {

    
    
    
    
    @IBAction func logoutBtn(_ sender: UIButton) {
        Model.instance.signOut()

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        ModelNotification.Login.observe(callback: { (value) in
            print("logout in view controller: ModelNotification ")
            if value! {
                print("logout view controller: ModelNotification : user is not loged out")
                
            }
            else {
                print("logout view controller: ModelNotification : user is  loged out")
                ModelNotification.removeObserver(observer: self)
                let secondVC:ViewControllerLogIn = UIStoryboard(name:"Main",bundle:nil).instantiateViewController(withIdentifier: "Login") as! ViewControllerLogIn
                self.navigationController?.pushViewController(secondVC, animated: true)
                self.dismiss(animated: true, completion: nil)

            }
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
   

}
