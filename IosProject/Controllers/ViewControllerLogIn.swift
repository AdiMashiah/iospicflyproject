//

//  ViewControllerLogIn.swift

//  IosProject

//

//  Created by Adi Mashiah on 19/12/2017.

//  Copyright © 2017 Adi Mashiah. All rights reserved.

//



import UIKit



class ViewControllerLogIn: UIViewController {
    

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
     var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    
    var email:String?
    var password:String?
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        activityIndicator.frame = CGRect(x:0,y:0,width:50,height:50)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        activityIndicator.center = self.view.center
        
        view.addSubview(activityIndicator)
        
        ModelNotification.Login.observe(callback: { (value) in
            print("login in view controller: ModelNotification ")
            self.activityIndicator.stopAnimating()
            if value! {
                print("login view controller: ModelNotification : user is logedin")
                ModelNotification.removeObserver(observer: self)
                self.dismiss(animated: true, completion: nil)
            }
            else {
                 print("login view controller: ModelNotification : user is not logedin")
                self.displayAnAlertMessage(userMessage: "problem signing in");
            }
        })
 
    }
    
    
    @IBAction func logIn(_ sender: Any) {
        
        email = emailTextField.text!
        password = passwordTextField.text!
        
        if(email != "" &&  password != "" && self.isValidEmail(testStr: email!)){
            self.activityIndicator.startAnimating()
            Model.instance.signinUser(email: email!, password: password!)
        }
        else {
            
          self.displayAnAlertMessage(userMessage: "you are missing input");
        }
        
    }
    

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()

        
    }

    
    func displayAnAlertMessage(userMessage:String){
        
        let myAlert = UIAlertController(title:"",message: userMessage, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title:"ok",style: .default, handler: nil)
        
        myAlert.addAction(okAction)

        present(myAlert,animated:true,completion:nil)
        
    }
    
    //checks if the email is valid
    
    private func isValidEmail(testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: testStr)
        
    }
    
    
 
    
  
    
}
