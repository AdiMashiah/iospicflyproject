//
//  TableViewControllerFavorite.swift
//  IosProject
//
//  Created by Adi Mashiah on 19/12/2017.
//  Copyright © 2017 Adi Mashiah. All rights reserved.
//

import UIKit


class TableViewControllerFavorite: UITableViewController {


    var favoriteIndex=0
    var places : [Place]?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        places = Model.instance.getAllFavoritesPlaces()
        
        ModelNotification.FavoritePlaceAdded.observe(callback: { (value) in
            print("ModelNotification new data in favorites view controller")
            if value != nil {
                print("recived new data in favorites view controller")
                self.places!.insert(value!, at: 0)
                self.loadView()
            }
            else {
                print("not recived new data in favorites view controller")
            }
        })
        
        ModelNotification.FavoritePlaceRemoved.observe(callback: { (value) in
            print("ModelNotification remove favorites view controller")
            if value != nil {
                print("recived remove in favorites view controller")
                var counter = 0
                var placeToRemoveIndex = 0
                for place in self.places! {
                    if  place.placeId! == value! {
                        placeToRemoveIndex = counter
                        break
                    }
                     counter+=1
                }
                self.places!.remove(at: placeToRemoveIndex)
                self.loadView()
            }
            else {
                print("not recived remove in favorites view controller")
                
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return places!.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
        let cell:TableViewCellFavorites = tableView.dequeueReusableCell(withIdentifier: "favoriteCell", for: indexPath) as! TableViewCellFavorites
        
        let content = places![indexPath.row]

        cell.placeImageView.image = content.image
        cell.placeNameLabel.text = content.name
        cell.placeDescriptionLabel.text = content.description;
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        favoriteIndex=indexPath.row
        performSegue(withIdentifier: "favoriteSegue", sender: self)
        
    }
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is ViewControllerPlaceDetails
        {
            let vc = segue.destination as? ViewControllerPlaceDetails
            vc?.placeDetails = places![favoriteIndex]
        }
    }


}
