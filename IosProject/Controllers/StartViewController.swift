//
//  StartViewController.swift
//  IosProject
//
//  Created by Adi Mashiah on 10/01/2018.
//  Copyright © 2018 Adi Mashiah. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
    

    override func viewDidLoad() {
    
        super.viewDidLoad()

       //Model.instance.signOut()

        ModelNotification.Login.observe(callback: { (value) in
            
            print("start view controller: ModelNotification")
            ModelNotification.removeObserver(observer: self)
            if value! {
                print("start view controller: ModelNotification : user is loged in")
             
                //self.activityIndicator.stopAnimating()
                self.performSegue(withIdentifier: "appSegue", sender: self)
            }
            else {
                 print("start view controller: ModelNotification : user is not loged in ******")              
                self.performSegue(withIdentifier: "loginSegue", sender: self)
            }
        })

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
            Model.instance.authenticateUser()
        
    }
    
    @IBAction func backafterLogin(segue:UIStoryboardSegue){
        
    }
    
    



    
}
