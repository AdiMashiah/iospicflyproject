//
//  ViewControllerPlaceDetails.swift
//  IosProject
//
//  Created by Adi Mashiah on 19/12/2017.
//  Copyright © 2017 Adi Mashiah. All rights reserved.
//

import UIKit

class ViewControllerPlaceDetails: UIViewController {

    
    var vcImportImage : Bool = false
    var placeDetails : Place!
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    

    @IBOutlet weak var favoriteBtn: UIButton!
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var placeImageView: UIImageView!
    @IBOutlet weak var placeDescriptionLabel: UILabel!
    @IBOutlet weak var sharePlaceBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       self.tabBarController?.tabBar.isHidden=true
        
        self.activityIndicator.frame = CGRect(x:0,y:0,width:100,height:100)
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.activityIndicator.center = self.view.center
        
        
        view.addSubview(self.activityIndicator)
        
        if self.placeDetails != nil {
            self.placeNameLabel.text = self.placeDetails!.name
            self.placeImageView.image = self.placeDetails!.image!
            self.placeDescriptionLabel.text = self.placeDetails!.description

        }
        
        if vcImportImage {
            self.sharePlaceBtn.isHidden = false
            self.favoriteBtn.isHidden = true
        }
        else {
              self.sharePlaceBtn.isHidden = true
        }

        if placeDetails.placeId != nil && Model.instance.isPlaceInFavorites(placeID:placeDetails.placeId!) {
            self.favoriteBtn.setImage(#imageLiteral(resourceName: "favorite_full"), for: .normal)
        }
        else {
            self.favoriteBtn.setImage(#imageLiteral(resourceName: "favorite_empty"), for: .normal)
        }
        
        ModelNotification.SharePlace.observe(callback: { (value) in
            self.activityIndicator.stopAnimating()
            if value! {
                // go to home page
                ModelNotification.removeObserver(observer: self)
                print("place was shared")
                self.navigationController?.popViewController(animated: true)
            }
            else {
                //pop up "cant share"
                print("cant share this place")
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func favoriteBtn(_ sender: UIButton) {
        if self.favoriteBtn.currentImage == #imageLiteral(resourceName: "favorite_empty") {
             self.favoriteBtn.setImage(#imageLiteral(resourceName: "favorite_full"), for: .normal)
            Model.instance.addPlaceToFavorites(place: self.placeDetails!)
        }
        else {
              self.favoriteBtn.setImage(#imageLiteral(resourceName: "favorite_empty"), for: .normal)
                Model.instance.removePlaceFromFavorites(placeID:placeDetails.placeId!)
        }
        self.reloadInputViews()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden=false

    }
    
    @IBAction func sharePlaceBtn(_ sender: UIButton) {

        activityIndicator.startAnimating()
        if self.placeDetails != nil {
             self.sharePlaceBtn.isEnabled = false
            Model.instance.addNewPlace(place: self.placeDetails!)

        }
        self.favoriteBtn.isEnabled = true

    }
    

}
