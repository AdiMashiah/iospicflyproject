//
//  TableViewControllerHome.swift
//  IosProject
//
//  Created by Adi Mashiah on 19/12/2017.
//  Copyright © 2017 Adi Mashiah. All rights reserved.
//

import UIKit



class TableViewControllerHome: UITableViewController {

    var myIndex=0
    var places : [Place]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
         places = Model.instance.getAllPlaces();
        ModelNotification.Place.observe(callback: { (value) in
            print("ModelNotification new data in main view controller")
            if value != nil {
                print("recived new data in main view controller")
                self.places!.insert(value!, at: 0)
                self.loadView()
            }
            else {
                print("not recived new data in main view controller")
                
            }
        })
    }


 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return places!.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell:TableViewCellHome = tableView.dequeueReusableCell(withIdentifier: "homeCell", for: indexPath) as! TableViewCellHome

         let content = places![indexPath.row]

 

        cell.PublisherNameLabel.text = content.publisher!;
        cell.placePictureImageView.image = content.image
        cell.placeNameLabel.text = content.name
        cell.placeDescriptionLabel.text = content.description
        return cell
        
    }
 
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        myIndex=indexPath.row
        performSegue(withIdentifier: "homeSegue", sender: self)
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is ViewControllerPlaceDetails
        {
            let vc = segue.destination as? ViewControllerPlaceDetails
            vc?.placeDetails = places![myIndex]
        }
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        ModelNotification.removeObserver(observer: self)
    }


}
