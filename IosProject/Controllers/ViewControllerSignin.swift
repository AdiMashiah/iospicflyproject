//
//  ViewControllerSignin.swift
//  IosProject
//
//  Created by Adi Mashiah on 19/12/2017.
//  Copyright © 2017 Adi Mashiah. All rights reserved.
//

import UIKit

class ViewControllerSignin: UIViewController {


    
    @IBOutlet weak var userNameTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var retypeTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
     var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    
    var email:String?
    var password:String?
    var retypePassword:String?
    var userName:String?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        
        activityIndicator.frame = CGRect(x:0,y:0,width:50,height:50)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        activityIndicator.center = self.view.center
        
        view.addSubview(activityIndicator)

        ModelNotification.Register.observe(callback: { (value) in
         self.activityIndicator.stopAnimating()
            
            if value! {
                 print("sign in view controller: ModelNotification : user is registered")
                ModelNotification.removeObserver(observer: self)
                self.dismiss(animated: true, completion: nil)
            }
            else {
                print("sign in view controller: ModelNotification : user is not registered")
                self.displayAnAlertMessage(userMessage: "problem register...");
            }
        })

    }


    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signinBtn(_ sender: UIButton) {
      
        userName = userNameTextField.text!
        email = emailTextField.text!
        password = passwordTextField.text!
        retypePassword = retypeTextField.text!
        
        if(userName == "" || email == "" ||  password == "" || retypePassword == ""){
            print("in the else function")
            displayAnAlertMessage(userMessage: "All fields are required");
            return
        }
     
        let isValid = isValidEmail(testStr: email!)
        if(isValid == false){
            displayAnAlertMessage(userMessage: "invalid email,please try again");
            return
            
        }
        if(password != retypePassword){
            displayAnAlertMessage(userMessage: "Passwords do not match");
            return
        }
        
        if(password!.count<6){
             displayAnAlertMessage(userMessage: "Passwords must contain at least 6 charachters");
            return
        }
        
        activityIndicator.startAnimating()
        Model.instance.registerUser(name: userName!, email: email!, password: password!)
        
    }
    
    
    
    
    
    
    func displayAnAlertMessage(userMessage:String){
        let myAlert = UIAlertController(title:"",message: userMessage, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title:"ok",style: .default, handler: nil)
        
        myAlert.addAction(okAction)
        
     
        present(myAlert,animated:true,completion:nil)
        
    }
    
    
    func isValidEmail(testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: testStr)
        
    }
    
    

}
