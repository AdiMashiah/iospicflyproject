//
//  User.swift
//  IosProject
//
//  Created by Dror Manzur on 31/12/2017.
//  Copyright © 2017 Adi Mashiah. All rights reserved.
//

import Foundation

class User {
    
    var userId:String?
    var name:String?
    var email:String?
    var lastUpdateDate:Date?
    
    static let instance = User()
    
    private init() {
        
    }
    
    init(userId:String, name:String, email:String) {
        self.userId = userId
        self.name = name
        self.email = email
    }
    
    init(userId:String, name:String, email:String,lastUpdateDate:Date) {
        self.userId = userId
        self.name = name
        self.email = email
        self.lastUpdateDate = lastUpdateDate
    }
    
    init(json:Dictionary<String,Any>){
        self.userId = json["id"] as? String
        self.name = json["name"] as? String
        self.email = json["email"] as? String
        if let ts = json["lastUpdateDate"] as? Double {
            self.lastUpdateDate = Date.fromFirebase(ts)
        }
    }
    
     func userToFirebase() -> Dictionary<String,Any> {
        var json = Dictionary<String,Any>()
        json["userId"] = self.userId
        json["name"] = self.name
        json["email"] = self.email
        return json
    }
    
    
    //change the values of the user from fire base
     func userFromJson(value:NSDictionary) {
        self.userId = value["id"] as? String
        self.name = value["name"] as? String
        self.email =  value["email"] as? String
        if let ts = value["lastUpdateDate"] as? Double {
            self.lastUpdateDate = Date.fromFirebase(ts)
         }
        
    }
    
}

