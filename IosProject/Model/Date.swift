//
//  Date.swift
//  IosProject
//
//  Created by Dror Manzur on 09/01/2018.
//  Copyright © 2018 Adi Mashiah. All rights reserved.
//

import Foundation

extension Date {
    
    var stringValue: String {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.string(from: self)
    }
    
    
    func toFirebase()->Double{
     
        return self.timeIntervalSince1970 * 1000
    }
    
    static func fromString(str:String)->Date {
        
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    
        let date = formatter.date(from:str)!
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date)
        let finalDate = calendar.date(from:components)
        return finalDate!
    }
    
    static func fromFirebase(_ interval:String)->Date{
        return Date(timeIntervalSince1970: Double(interval)!)
    }
    
    static func fromFirebase(_ interval:Double)->Date{
        if (interval>9999999999){
            return Date(timeIntervalSince1970: interval/1000)
        }else{
            return Date(timeIntervalSince1970: interval)
        }
    }
    
    
    
}

