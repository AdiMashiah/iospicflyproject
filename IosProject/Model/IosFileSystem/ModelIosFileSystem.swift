//
//  ModelIosFileSystem.swift
//  IosProject
//
//  Created by Dror Manzur on 31/12/2017.
//  Copyright © 2017 Adi Mashiah. All rights reserved.
//

import Foundation
import UIKit

class ModelIosFileSystem {
    
    let allPlacesPath:URL
    let favoritesPlacesPath:URL
    
    init(){
        let picturesDirectory = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.picturesDirectory, .userDomainMask, true)[0])
        let allPlacesPath = picturesDirectory.appendingPathComponent("allplaces")
        let favoritesPlacesPath = picturesDirectory.appendingPathComponent("favoritesplaces")
        print(allPlacesPath!)
        print(favoritesPlacesPath!)

        self.allPlacesPath = allPlacesPath!
        self.favoritesPlacesPath = favoritesPlacesPath!
        do {
            try FileManager.default.createDirectory(atPath: allPlacesPath!.path, withIntermediateDirectories: true, attributes: nil)
            try FileManager.default.createDirectory(atPath: favoritesPlacesPath!.path, withIntermediateDirectories: true, attributes: nil)

        } catch let error as NSError {
            NSLog("Unable to create directory \(error.debugDescription)")
        }
    }

    func addPlaceImageToAllPlaces(name:String,img:UIImage){
        
        let fileName = name + ".png"
        if let data = UIImageJPEGRepresentation(img, 0.8) {
            let filename = self.allPlacesPath.appendingPathComponent(fileName)
            try? data.write(to: filename)
            print("place image was saved in all places dir in file system")

        }
    }
    
    func addPlaceImageToFavoritesPlaces(name:String,img:UIImage){
        let fileName = name + ".png"
        if let data = UIImageJPEGRepresentation(img,0.8) {
            let filename = self.favoritesPlacesPath.appendingPathComponent(fileName)
            try? data.write(to: filename)
            print("place image was saved in favorite places dir in file system")
        }
    }
    
    

    func getPlaceImageFromAllPlacesDir(name:String)->UIImage?{
        
        let fileName = name + ".png"
        let filePath = self.allPlacesPath.appendingPathComponent(fileName)

        if (FileManager.default.fileExists(atPath: filePath.path)){
            let readImage = UIImage.init(contentsOfFile: filePath.path)
            print("place image was read from all places dir in file system")
            return readImage
        }
        else {
             print("getPlaceImageFromAllPlacesDir fail")
              return nil
        }
      
    }
    
    func getPlaceImageFromFavoritePlacesDir(name:String)->UIImage?{
        
        let fileName = name + ".png"
        let filePath = self.favoritesPlacesPath.appendingPathComponent(fileName)
        if (FileManager.default.fileExists(atPath: filePath.path)){
            let readImage = UIImage.init(contentsOfFile: filePath.path)
            print("place image was read from favorites places dir in file system")
            return readImage
        }
        else {
            print("getPlaceImageFromAllPlacesDir fail")
            return nil
        }
    }
    
    
}
