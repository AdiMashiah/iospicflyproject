//
//  FirebaseAuth.swift
//  IosProject
//
//  Created by Dror Manzur on 01/01/2018.
//  Copyright © 2018 Adi Mashiah. All rights reserved.
//gggggghh

import FirebaseAuth
import Foundation

class FirebaseAuth {
    
    let authenticationRef: Auth!
    
    
    init(){
        self.authenticationRef = Auth.auth()
    }
    
    
    func signUserOut(){
        do {
            try self.authenticationRef.signOut()
            print("user signed out in FirebaseAuth")
        }catch {
            print("error signed out user in FirebaseAuth")
        }
    }
    
    func isUserSignedin() -> String? {
        if let currentUser = self.authenticationRef.currentUser {
            print("user is signedin in firebase auth with id: " + currentUser.uid)
            return currentUser.uid
        }
        else {
            print("user is not signedin in firebase auth")
            return nil
        }
    }
    
    func registerUser(name:String,email:String,password:String,callback:@escaping (Bool)->Void) {
        self.authenticationRef.createUser(withEmail: email, password: password,completion: { (regUser, error) in
            if error != nil {
                print("error register user in firebase auth")
                callback(false)
            }
            else {
                let userID = regUser?.uid
                User.instance.userId = userID
                User.instance.name = name
                User.instance.email = email
                print("user was registered in firebase auth")
                callback(true);
            }
        })
    }
    
    func signinUser(email:String,password:String,callback:@escaping (String?)->Void) {
        self.authenticationRef.signIn(withEmail: email, password: password,completion: { (user, error) in
            if error != nil {
                print("error signin user in firebase auth")
                callback(nil)
            }
            else {
                print("user was signedin in firebase auth")
                callback(user?.uid)
            }
        })
    }
    
    
}

