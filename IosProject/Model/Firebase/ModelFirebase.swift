//
//  ModelFirebase.swift
//  IosProject
//
//  Created by Dror Manzur on 01/01/2018.hhh

//

import FirebaseAuth
import Foundation
import SwiftSoup


class ModelFirebase {
    
    let firebaseAuth:FirebaseAuth
    let firebaseStorage:FirebaseStorage
    let firebaseDatabase:FirebaseDatabase

    
    init(){
        self.firebaseAuth = FirebaseAuth()
        self.firebaseStorage = FirebaseStorage()
        self.firebaseDatabase = FirebaseDatabase()
    }
    
    func isUserSignedin() -> String? {
        return self.firebaseAuth.isUserSignedin()
    }
    
    
     func searchImage(image: UIImage,modelCallback:@escaping (String?)->Void){
        
        //save image to look for in firebase storage
        self.firebaseStorage.saveImageToLookForToFirebase(image: image, callback: { (imageURL) in
            if let imageURL = imageURL {
                modelCallback(imageURL)
            }
            else {
                modelCallback(nil)
            }
        })
     }
    
    //firebase functions
    func registerUser(name: String, email:String, password:String,modelCallback:@escaping (Bool)->Void){
        
        self.firebaseAuth.registerUser(name:name,email:email,password:password,callback : { (isRegistered:Bool) in
            if isRegistered {
                //lets add user to firebase database
                self.firebaseDatabase.addUser(callback: { (isAdded:Bool) in
                    if isAdded {
                       modelCallback(true)
                    }
                    else {
                        modelCallback(false)
                    }
                })
            }
            else {
                modelCallback(false)
            }
        })
        
    }
    
    func signUserOut(){
        self.firebaseDatabase.removeObservers()
        self.firebaseAuth.signUserOut()
    }
    
    func signinUser(email:String, password:String,modelCallback:@escaping (String?)->Void){
        self.firebaseAuth.signinUser(email:email,password:password,callback : { (userID:String?) in
            if let userID = userID {
                modelCallback(userID)
                //self.firebaseDatabase.getUserById(id: userID)
            }
            else {
                 modelCallback(nil)
            }
        })
    }
    
    
    func addFavoritePlaceIdToUser(userID:String,placeID:String){
        self.firebaseDatabase.addFavoritePlaceIdToUser(userID:userID,placeID: placeID)
    }
    
    func removeFavoritePlaceIdToUser(userID:String,placeID:String){
        self.firebaseDatabase.removeFavoritePlaceIdToUser(userID:userID,placeID: placeID)
    }
    
    
    func getFavoritesPlacesOfUser(userID:String,modelCallback:@escaping (Place?)->Void){
        self.firebaseDatabase.getFavoritesPlacesOfUser(userID: userID,callback: { (place:Place?) in
            if let place = place {
                self.firebaseStorage.getPlaceImageFromFirebase(place: place,modelFirebaseCallback : { (place) in
                    if let place = place {
                        modelCallback(place)
                    }
                })
            }
        })
    }
    
    func addNewPlace(place:Place,modelCallback:@escaping (Bool)->Void){
        
        let placeKey = self.firebaseDatabase.getPlaceKey()
        place.placeId = placeKey
        self.firebaseStorage.saveImageToFirebase(image: place.image!, name: place.placeId!, callback: { (imageURL) in
            if imageURL != nil {
                place.imageURL = imageURL
                self.firebaseDatabase.addNewPlace(place: place)
                modelCallback(true)
            }
            else {
                modelCallback(false)
            }
        })
    }
    
    func getNewPlacesAndObserve(modelCallback:@escaping (Place)->Void){
        self.firebaseDatabase.getNewPlacesAndObserve(modelFirebaseCallback : { (place) in
            self.firebaseStorage.getPlaceImageFromFirebase(place: place,modelFirebaseCallback : { (newPlace) in
                if newPlace != nil {
                    modelCallback(newPlace!)
                }
            })
        })
    }
    
    func updateUserLastUpdateDate() {
        self.firebaseDatabase.updateUserLastUpdateDate()
    }
    
    func removeObservers(){
        self.removeObservers()
    }
    
    
    func getPlaceImageFromFirebase(place:Place,modelFirebaseCallback:@escaping (Place?)->Void){
        
        self.firebaseStorage.getPlaceImageFromFirebase(place: place, modelFirebaseCallback: { (placeNew) in
            modelFirebaseCallback(placeNew)
       })
  
    }
    

    func getUserDetailsByID(id:String,callback:@escaping (Bool)->Void){
        self.firebaseDatabase.getUserById(id: id, callback: callback)
    }

}

