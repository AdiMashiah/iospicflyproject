//
//  FirebaseDatabase.swift
//  IosProject
//
//  Created by Dror Manzur on 01/01/2018.
//  Copyright © 2018 Adi Mashiah. All rights reserved.hhhh
//
import FirebaseDatabase
import Foundation

class FirebaseDatabase {
    
    let databaseReference: DatabaseReference!
    let usersPath = "users"
    let placesPath = "places"
    let userFavoritePlacePath = "favorites"
    let userLastUpdateDatePath = "lastUpdateDate"
    
    init(){
        self.databaseReference = Database.database().reference();
    }
    
    //user functions
    func addUser(callback:@escaping (Bool)->Void){
        let userRef = self.databaseReference.child(usersPath).child(User.instance.userId!)
        var json = User.instance.userToFirebase()
        json["lastUpdateDate"] = ServerValue.timestamp()
        userRef.setValue(json)
        
        
        userRef.observeSingleEvent(of: .value, with: {(snapshot) in
            if let jsonTs = snapshot.value as? Dictionary<String,Any> {
                let time = Date.fromFirebase(jsonTs["lastUpdateDate"] as! Double)
                User.instance.lastUpdateDate = time
                print("user was added to firebase database with date: \(User.instance.lastUpdateDate!)")
                callback(true)
            }
            else {
                print("problem getting timestamp")
                callback(false)
            }
        })
        
    }
    
    func updateUserLastUpdateDate(){
        let usersRef = self.databaseReference.child(usersPath).child(User.instance.userId!).child(userLastUpdateDatePath)
        usersRef.setValue(User.instance.lastUpdateDate!.toFirebase())
        print("user lastUpdateDate was updated in firebase database with date: + \(User.instance.lastUpdateDate!)")
 
    }
    
 

    //get the info of the user from firebase by id
    func getUserById(id:String,callback:@escaping (Bool)->Void){
   
        let ref = self.databaseReference.child(usersPath).child(id)
        ref.observeSingleEvent(of: .value, with: {(snapshot) in
            if (snapshot.exists()){
                let value = snapshot.value as? NSDictionary
                User.instance.userFromJson(value: value!)
                callback(true)
            }
            else {
                callback(false)
            }
        })
    }
    
    func addFavoritePlaceIdToUser(userID:String,placeID:String) {
        let userFavoritePlacesRef = self.databaseReference.child(usersPath).child(userID).child(userFavoritePlacePath).child(placeID)
        let key = placeID
        
        var json = Dictionary<String,Any>()
        json[key] = placeID
        
        userFavoritePlacesRef.setValue(json)
        print("user favorite place was added to firebase database")
        
    }
    
    func removeFavoritePlaceIdToUser(userID:String,placeID: String) {
        let userFavoritePlaceRef = self.databaseReference.child(usersPath).child(userID).child(userFavoritePlacePath).child(placeID)
        userFavoritePlaceRef.removeValue()
        print("user favorite place was removed from firebase database")
    }
    
    func getFavoritesPlacesOfUser(userID:String, callback:@escaping (Place?)->Void){
        let ref = self.databaseReference.child(usersPath).child(userID).child(userFavoritePlacePath)
        print(userID)
        ref.observeSingleEvent(of: .value, with: {(snapshot) in
            if (snapshot.exists()){
                print("snapshot exist")

                let json = snapshot.value as! Dictionary<String,Any>
                print("got user favorite place ids from firebase database")
                self.getPlacesByIDs(placesIDs:json,callback : callback)
            }
            else {
                print("snapshot not exist")

            }
        })
    }
    

    
            

    private func getPlacesByIDs(placesIDs:[String:Any], callback:@escaping (Place?)->Void){
        if (!placesIDs.isEmpty){
            for (key,value) in placesIDs {
                print(key)
                let placeRef = self.databaseReference.child(self.placesPath).child(key)
                placeRef.observeSingleEvent(of: .value, with: {(snapshot) in
                    let json = snapshot.value as! Dictionary<String,Any>
                    let place = Place(json: json)
                    print("got user favorite place from firebase database")
                    callback(place)
                })
            }
        }
        else {
            callback(nil)
        }
    }
    
    func getPlaceKey()->String {
        let placesRef = self.databaseReference.child(placesPath).childByAutoId()
        return placesRef.key
        
    }
    
    func addNewPlace(place:Place) {
        let placesRef = self.databaseReference.child(placesPath).child(place.placeId!)
        var json = Place.placeToFirebase(place:place)
        json["timestamp"] = ServerValue.timestamp()
        placesRef.setValue(json)
        print("new place was added to firebase database")
    }
    
    
    func getNewPlacesAndObserve(modelFirebaseCallback:@escaping (Place)->Void){
        let handler = {(snapshot:DataSnapshot) in
           let json = snapshot.value as! Dictionary<String,Any>
                let newPlace = Place(json: json)
                if (Int(newPlace.timestamp!.toFirebase()/1000) > Int(User.instance.lastUpdateDate!.toFirebase()/1000)){
                    print("firebase database: new place arrived with id: \(newPlace.placeId!) and timestamp: \(newPlace.timestamp!)")
                    modelFirebaseCallback(newPlace)
                }
        }
        
        let ref = self.databaseReference.child(self.placesPath)    
        let fbQuery = ref.queryOrdered(byChild:"timestamp").queryStarting(atValue:User.instance.lastUpdateDate!.toFirebase())
        fbQuery.observe(DataEventType.childAdded, with: handler)
        
    }
    
    func removeObservers(){
        let ref = self.databaseReference.child(self.placesPath)
        ref.removeAllObservers()
    }
}

