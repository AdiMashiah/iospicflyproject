//
//  FirebaseStorage.swift
//  IosProject
//
//  Created by Dror Manzur on 01/01/2018.
//  Copyright © 2018 Adi Mashiah. All rights reserved.hh
//
import FirebaseStorage
import Foundation
import UIKit

class FirebaseStorage {
    
    let storageReference: StorageReference!
    
    init(){
        self.storageReference = Storage.storage().reference()
    }
    
    func getPlaceImageFromFirebase(place:Place,modelFirebaseCallback:@escaping (Place?)->Void){
        let ref = storageReference.child(place.placeId!)
        
    
       
        ref.getData(maxSize: 10000000, completion: {(data, error) in
            if (error == nil && data != nil){
                let image = UIImage(data: data!)
                place.image = image
                print("got place image from firebase storage")
                modelFirebaseCallback(place)
            }else{
                //notify new place received with error image
                print("error receiving place image from firebase storage")
                modelFirebaseCallback(nil)
            }
        })
 

    }
    
    
    func saveImageToLookForToFirebase(image:UIImage, callback:@escaping (String?)->Void){
        let name = User.instance.userId! + "temp"
        let fileRef = storageReference.child(name)  
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        if let data = UIImageJPEGRepresentation(image, 0.8) {
            fileRef.putData(data,metadata: metadata){metadata, error in
                if (error != nil) {
                    print("error saveImageToLookForToFirebase")
                    callback(nil)
                } else {
                    let downloadURL = metadata!.downloadURL()
                    print("good saveImageToLookForToFirebase")
                    callback(downloadURL?.absoluteString)
                }
            }
        }
    }
    
    func saveImageToFirebase(image:UIImage, name:String, callback:@escaping (String?)->Void){
        let fileRef = storageReference.child(name)
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        if let data = UIImageJPEGRepresentation(image, 0.8) {
            fileRef.putData(data,metadata: metadata){metadata, error in
                if (error != nil) {
                    print("error saving place image in firebase storage")
                    callback(nil)
                } else {
                    let downloadURL = metadata!.downloadURL()
                    print("image was saved in firebase storage")
                    callback(downloadURL?.absoluteString)
                }
            }
        }
    }
}

