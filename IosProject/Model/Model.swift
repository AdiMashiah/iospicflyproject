//
//  Model.swift
//  IosProject
//
//  Created by Dror Manzur on 31/12/2017.
//  Copyright © 2017 Adi Mashiah. All rights reserved.
//change
//

import Foundation
import UIKit


class ModelNotificationBase<T>{
    var name:String?

    init(name:String){
        self.name = name
    }
    
    func observe(callback:@escaping (T?)->Void)->Any{
        return NotificationCenter.default.addObserver(forName: NSNotification.Name(name!), object: nil, queue: nil) { (data) in
            if let data = data.userInfo?["data"] as? T {
                callback(data)
            }
        }
    }
    
    func post(data:T){
        NotificationCenter.default.post(name: NSNotification.Name(name!), object: self, userInfo: ["data":data])
    }
}

class ModelNotification{
    
    static let Place = ModelNotificationBase<Place>(name: "PlaceNotification")
    static let FavoritePlaceAdded = ModelNotificationBase<Place>(name: "FavoritePlaceAddedNotification")
    static let FavoritePlaceRemoved = ModelNotificationBase<String>(name: "FavoritePlaceRemovedNotification")
    static let Login = ModelNotificationBase<Bool>(name: "LoginNotification")
    static let Register = ModelNotificationBase<Bool>(name: "RegisterNotification")
    static let GetInfoFromGoogle = ModelNotificationBase<String?>(name: "GetInfoFromGoogleNotification")
    static let SharePlace = ModelNotificationBase<Bool>(name: "SharePlaceNotification")
    
    static func removeObserver(observer:Any){
        NotificationCenter.default.removeObserver(observer)
        print("observer removed")
    }
}

class Model{
    
    static let instance = Model()
    
    let modelSql = ModelSql()
    let modelFirebase = ModelFirebase()
    let modelIosFileSystem = ModelIosFileSystem()
    
    
    private init() { }
    
    //checks if the user is login
    func authenticateUser(){
        let uid = self.modelFirebase.isUserSignedin()
        if let uid = uid {
            //checks if the user is exist in sql table
            let ans = self.modelSql.isUserExsit(userId: uid)
            if ans == true{
                self.self.modelSql.setUserDetails(userID: uid)
                self.getNewPlacesAndObserve()
                ModelNotification.Login.post(data: true)
            }
            else{
                self.signOut()
            }
        }
        else {
             ModelNotification.Login.post(data: false)
        }
        
    }
    
    
    //sign out the user
    func signOut(){
        self.modelFirebase.signUserOut()
        ModelNotification.Login.post(data: false)
        
    }
    
    
    ////////////////////
    func getAllFavoritesPlaces()->[Place] {
        
        let places = self.modelSql.getAllFavoritePlaces()!
        for place in places {
            place.image = self.modelIosFileSystem.getPlaceImageFromAllPlacesDir(name: place.placeId!)
        }
        return places
    }
    /////////////////
    
    //register user
    func registerUser(name:String,email:String,password:String){
        self.modelFirebase.registerUser(name:name,email:email,password:password,modelCallback : { (isRegistered:Bool) in
            if isRegistered {
                self.modelSql.addUser()
                ModelNotification.Register.post(data: true)
            }
            else {
                ModelNotification.Register.post(data: false)
            }
        })
    }
    
    func isPlaceInFavorites(placeID:String)->Bool {
        return self.modelSql.isPlaceInFavorites(placeID:placeID)
    }
    
    
    
    //remove favorite place from sql and firebase
    func removePlaceFromFavorites(placeID:String){

        self.modelFirebase.removeFavoritePlaceIdToUser(userID: User.instance.userId!, placeID: placeID)
        self.modelSql.removePlaceFromFavorites(placeID: placeID)
        ModelNotification.FavoritePlaceRemoved.post(data: placeID)
    }
    
    //add place to favorites in firebase and in sql
    func addPlaceToFavorites(place:Place){
        self.modelFirebase.addFavoritePlaceIdToUser(userID: User.instance.userId!, placeID: place.placeId!)
        self.modelSql.addPlaceToFavorite(place: place)
        ModelNotification.FavoritePlaceAdded.post(data: place)
        
    }
    
    //need to edit last
    func signinUser(email:String,password:String){
        self.modelFirebase.signinUser(email:email,password:password,modelCallback : { (userID:String?) in

            if let userID = userID {
                let ans = self.modelSql.isUserExsit(userId: userID)
                if ans == true{
                    self.modelSql.setUserDetails(userID: userID)
                     ModelNotification.Login.post(data: true)
                }
                else{
                    //get the info of the user from the firebase
                    self.modelFirebase.getUserDetailsByID(id: userID, callback: { (userIsExist:Bool) in
                        if userIsExist{
                            User.instance.userId = userID
                             self.modelSql.addUser()
                            self.modelFirebase.getFavoritesPlacesOfUser(userID: userID, modelCallback: { (place) in
                                if let place = place {
                                    self.modelSql.addPlace(place: place)
                                    self.modelIosFileSystem.addPlaceImageToAllPlaces(name: place.placeId!, img: place.image!)
                                    self.modelSql.addPlaceToFavorite(place: place)
                                    ModelNotification.Place.post(data: place)
                                    ModelNotification.FavoritePlaceAdded.post(data: place)
                                    
                                }
                            })
                            ModelNotification.Login.post(data: true)
                        }
                        else {
                            ModelNotification.Login.post(data: false)
                        }
                    })
                }
            }
            else {
                ModelNotification.Login.post(data: false)
            }

        })
    }
    
    
    func addNewPlace(place:Place) {
        place.publisher = User.instance.name!
        self.modelFirebase.addNewPlace(place: place,modelCallback: { (value) in
            //save place details in sql
            ModelNotification.SharePlace.post(data: value)
        })
        
    }
    
    
    func searchImage(image:UIImage) {

        self.modelFirebase.searchImage(image:image,modelCallback: { (imageURL) in
         ModelNotification.GetInfoFromGoogle.post(data:imageURL)
        })
    }
    
    private func getNewPlacesAndObserve(){
        self.modelFirebase.getNewPlacesAndObserve(modelCallback: { (newPlace) in
            self.modelSql.addPlace(place: newPlace)
            print("new place is arrived")

            let lastUpdateDate = newPlace.timestamp
            User.instance.lastUpdateDate = lastUpdateDate
            self.modelIosFileSystem.addPlaceImageToAllPlaces(name: newPlace.placeId!, img: newPlace.image!)
            self.modelSql.updateUserLastUpdateDate()
            self.modelFirebase.updateUserLastUpdateDate()
            print("model - new place was saved -  finish")
            //notify new place arrived
            ModelNotification.Place.post(data: newPlace)
            
        })
    }
    
    func getAllPlaces() -> [Place]{
        var places = [Place]()
        places = self.modelSql.getAllPlaces()!
            for place in places {
                place.image = self.modelIosFileSystem.getPlaceImageFromAllPlacesDir(name: place.placeId!)
            }
            return places
    }
    
}
