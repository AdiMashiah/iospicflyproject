//
//  GoogleResponse.swift
//  IosProject
//
//  Created by Dror Manzur on 04/02/2018.
//  Copyright © 2018 Adi Mashiah. All rights reserved.
//

import Foundation
import SwiftSoup

enum HTMLError: Error {
    case badInnerHTML
}

struct GoogleResponse {
    
    init(_ innerHTML: Any?) throws {
        print("try to parse to string with inner html: ")
        print("\(innerHTML)")
    guard let htmlString = innerHTML as? String else { throw HTMLError.badInnerHTML }
        print("html string is: \(htmlString)")
        let doc =  try SwiftSoup.parse(htmlString)
        print(doc)
        print("finish parse")
        let placeName = try doc.getElementsByClass("_gUb").text()
        print(placeName)
        //let placeDescription = try doc.getElementsByClass("pd").text()
   }
    
}

