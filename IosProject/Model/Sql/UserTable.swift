//
//  UserTable.swift
//  IosProject
//
//  Created by Dror Manzur on 31/12/2017.
//  Copyright © 2017 Adi Mashiah. All rights reserved.
//

import Foundation

class UserTable {
    
    
    let USER_TABLE = "USER"
    let USERID = "USERID"
    let NAME = "NAME"
    let EMAIL = "EMAIL"
    let LASTUPDATEDATE = "LASTUPDATEDATE"
    
    
    func createTable(toDB database:OpaquePointer?)->Bool{
        var errormsg: UnsafeMutablePointer<Int8>? = nil
        let res = sqlite3_exec(database, "CREATE TABLE IF NOT EXISTS " + USER_TABLE + " (" +
            USERID + " TEXT PRIMARY KEY, " +
            NAME + " TEXT, " +
            EMAIL + " TEXT, " +
            LASTUPDATEDATE + " TEXT)", nil, nil, &errormsg)
        if(res != 0){
            print("error creating table");
            return false
        }
        return true
    }
    
    func addUser(toDB database:OpaquePointer?){
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"INSERT OR REPLACE INTO " +
            USER_TABLE + " ( " +
            USERID + ", " +
            NAME + ", " +
            EMAIL + ", " +
            LASTUPDATEDATE + " ) VALUES (?,?,?,?);",-1,
                                           &sqlite3_stmt,nil) == SQLITE_OK){
            
            let id = User.instance.userId!.cString(using: .utf8)
            let name = User.instance.name!.cString(using: .utf8)
            let email = User.instance.email!.cString(using: .utf8)
            let lastUpdateDate = User.instance.lastUpdateDate!.stringValue.cString(using: .utf8)
      
            sqlite3_bind_text(sqlite3_stmt, 1, id,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 2, name,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 3, email,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 4, lastUpdateDate,-1,nil);
         

            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("user was added to user sql table with timestamp: \(User.instance.lastUpdateDate!)")
            }
            else{
                print("add user faild in user sql table")
            }
        }
        sqlite3_finalize(sqlite3_stmt)
    }
    
   
    
    
    func setUserDetails(fromDB database:OpaquePointer?, withId usId:String){

        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from " +
            USER_TABLE + " where " +
            USERID + " = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_text(sqlite3_stmt, 1, usId.cString(using: .utf8),-1,nil);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let uId  = String(cString:sqlite3_column_text(sqlite3_stmt,0))
                let name  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
                let email  = String(cString:sqlite3_column_text(sqlite3_stmt,2))
                let lastUpdateDate  = String(cString:sqlite3_column_text(sqlite3_stmt,3))
                
                User.instance.userId = uId
                User.instance.name = name
                User.instance.email = email
                User.instance.lastUpdateDate = Date.fromString(str:lastUpdateDate)
                
                print("UserDetails was set from user sql table with timestamp: \(User.instance.lastUpdateDate!)")
                
                
            }
            else {
                print("set UserDetails failed in user sql table")
            }
        }
        sqlite3_finalize(sqlite3_stmt)
    }
    
    
    func updateUserLastUpdateDate(fromDB database:OpaquePointer?){
   
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"INSERT OR REPLACE INTO " +
            USER_TABLE + " ( " +
            USERID + ", " +
            NAME + ", " +
            EMAIL + ", " +
            LASTUPDATEDATE + " ) VALUES (?,?,?,?);",-1,
                                                    &sqlite3_stmt,nil) == SQLITE_OK){
            
            let id = User.instance.userId!.cString(using: .utf8)
            let name = User.instance.name!.cString(using: .utf8)
            let email = User.instance.email!.cString(using: .utf8)
            let lastUpdateDate = User.instance.lastUpdateDate!.stringValue.cString(using: .utf8)
            
            sqlite3_bind_text(sqlite3_stmt, 1, id,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 2, name,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 3, email,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 4, lastUpdateDate,-1,nil);
            
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("user lastUpdateDate was updated in user sql table with timestamp: \(User.instance.lastUpdateDate!)")
            }
            else{
                print("user lastUpdateate update faild in user sql table")
            }
        }
        sqlite3_finalize(sqlite3_stmt)
    }
    
    
    //checks if the user is already exist in sql
    func isUserExist(romDB database:OpaquePointer?,userId:String)->Bool{
        
        var isUserExist = false
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from " +
            USER_TABLE + " where " +
            USERID + " = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_text(sqlite3_stmt, 1, userId.cString(using: .utf8),-1,nil);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
               isUserExist = true
                 print("user exist in users sql table")
            }
            else {
                print("user is not exist in users sql table")
                isUserExist = false
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return isUserExist
        
    }
    
    
}

