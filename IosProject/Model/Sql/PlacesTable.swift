//
//  PlacesTable.swift
//  IosProject
//
//  Created by Dror Manzur on 31/12/2017.
//  Copyright © 2017 Adi Mashiah. All rights reserved.
//

import Foundation

class PlacesTable {
    
    
    
    let PLACES_TABLE = "PLACES"
    let PLACEID = "PID"
    let NAME = "NAME"
    let DESCRIPTION = "DESCRIPTION"
    let PUBLISHER = "PUBLISHER"
    let IMAGEURL = "IMAGEURL"
    let TIMESTAMP = "TIMESTAMP"
    
    
    
    func createTable(toDB database:OpaquePointer?)->Bool{
        var errormsg: UnsafeMutablePointer<Int8>? = nil
        let res = sqlite3_exec(database, "CREATE TABLE IF NOT EXISTS " + PLACES_TABLE + " (" +
            PLACEID + " TEXT PRIMARY KEY, " +
            NAME + " TEXT, " +
            DESCRIPTION + " TEXT, " +
            PUBLISHER + " TEXT, " +
            IMAGEURL + " TEXT, " +
            TIMESTAMP + " TEXT)", nil, nil, &errormsg)
        if(res != 0){
            print("error creating table");
            return false
        }
        return true
    }
    
    func addPlace(toDB database:OpaquePointer?, place:Place){
        
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"INSERT OR REPLACE INTO " +
            PLACES_TABLE + " ( " +
            PLACEID + ", " +
            NAME + ", " +
            DESCRIPTION + ", " +
            PUBLISHER + ", " +
            IMAGEURL + ", " +
            TIMESTAMP + " ) VALUES (?,?,?,?,?,?);",-1,
                                                        &sqlite3_stmt,nil) == SQLITE_OK){
            
            let id = place.placeId!.cString(using: .utf8)
            let name = place.name.cString(using: .utf8)
            let description = place.description.cString(using: .utf8)
            let publisher = place.publisher!.cString(using: .utf8)
            let imageURL = place.imageURL!.cString(using: .utf8)
            let timestamp = place.timestamp!.stringValue.cString(using: .utf8)

            sqlite3_bind_text(sqlite3_stmt, 1, id,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 2, name,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 3, description,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 4, publisher,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 5, imageURL,-1,nil);
            sqlite3_bind_text(sqlite3_stmt, 6, timestamp,-1,nil);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_DONE){
                print("new place was added succefully to allPlaces sql with timestamp: \(place.timestamp!)")
            }
        }
        sqlite3_finalize(sqlite3_stmt)
    }
    
    
    
    func getPlace(fromDB database:OpaquePointer?, withId placeId:String)->Place?{
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from " +
            PLACES_TABLE + " where " +
            PLACEID + " = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_text(sqlite3_stmt, 1, placeId.cString(using: .utf8),-1,nil);
            
            if(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let pId  = String(cString:sqlite3_column_text(sqlite3_stmt,0))
                let name  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
                let description  = String(cString:sqlite3_column_text(sqlite3_stmt,2))
                let publisher  = String(cString:sqlite3_column_text(sqlite3_stmt,3))
                let imageURL = String(cString:sqlite3_column_text(sqlite3_stmt,4))
                let timestamp  = String(cString:sqlite3_column_text(sqlite3_stmt,5))

              print("get place from all places sql table")
                return Place(placeId: pId,name: name,description: description,publisher: publisher,imageURL : imageURL,timestamp:Date.fromString(str:timestamp))
            }
        }
        sqlite3_finalize(sqlite3_stmt)
        return nil
    }
    
    func getAllPlaces(fromDB database:OpaquePointer?)->[Place]{
        var places = [Place]()
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"SELECT * from " +
            PLACES_TABLE + ";",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            while(sqlite3_step(sqlite3_stmt) == SQLITE_ROW){
                let pId  = String(cString:sqlite3_column_text(sqlite3_stmt,0))
                let name  = String(cString:sqlite3_column_text(sqlite3_stmt,1))
                let description  = String(cString:sqlite3_column_text(sqlite3_stmt,2))
                let publisher  = String(cString:sqlite3_column_text(sqlite3_stmt,3))
                let imageURL  = String(cString:sqlite3_column_text(sqlite3_stmt,4))
                let timestamp  = String(cString:sqlite3_column_text(sqlite3_stmt,5))

              
                places.insert(Place(placeId: pId,name: name,description: description,publisher: publisher,imageURL : imageURL,timestamp:Date.fromString(str:timestamp)),at:0)
            }
        }
        sqlite3_finalize(sqlite3_stmt)
           print("get all places from allPlaces sql table")
        return places
    }
    
    func deletePlace(fromDB database:OpaquePointer?, withId placeId:String){
        var sqlite3_stmt: OpaquePointer? = nil
        if (sqlite3_prepare_v2(database,"delete from " +
            PLACES_TABLE + " where " +
            PLACEID + " = ?;",-1,&sqlite3_stmt,nil) == SQLITE_OK){
            
            sqlite3_bind_text(sqlite3_stmt, 1, placeId.cString(using: .utf8),-1,nil);
            
            if(sqlite3_step(sqlite3_stmt) != SQLITE_DONE){
                print ("failes executing deletePlace")
            }
        }
        sqlite3_finalize(sqlite3_stmt)
    }
}

