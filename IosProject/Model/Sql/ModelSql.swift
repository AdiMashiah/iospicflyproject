//
//  ModelSql.swift
//  IosProject
//
//  Created by Dror Manzur on 31/12/2017.
//  Copyright © 2017 Adi Mashiah. All rights reserved.
//

import Foundation


class ModelSql {
    
    
    
    var database: OpaquePointer? = nil

    
    let userTable = UserTable()
    let placesTable = PlacesTable()
    let favoritesPlacesTable = FavoritesTable()
    
    
    
    init() {
        //open the DB file
        let dbFileName = "database2.db"
        if let dir = FileManager.default.urls(for: .documentDirectory, in:
            .userDomainMask).first{
            let path = dir.appendingPathComponent(dbFileName)
            if sqlite3_open(path.absoluteString, &database) != SQLITE_OK {
                print("Failed to open db file: \(path.absoluteString)")
                return
            }
        }
        
        var result = userTable.createTable(toDB: database)
        
        result = result && self.placesTable.createTable(toDB: database)
        result = result && self.favoritesPlacesTable.createTable(toDB: database)
        if (!result){
            print("Failed to open one of the tables")
            return
        }
    }
    
    
    
    //user table functions
    func addUser(){
        self.userTable.addUser(toDB: database)
    }
    
    func setUserDetails(userID:String) {
         self.userTable.setUserDetails(fromDB: database, withId: userID)
    }
    
    func updateUserLastUpdateDate(){
        self.userTable.updateUserLastUpdateDate(fromDB: database)
    }
    
    
    //places table functions
    func addPlace(place:Place){
        self.placesTable.addPlace(toDB: database, place: place)
    }
    
    func getPlace(placeID:String)->Place? {
        return self.placesTable.getPlace(fromDB: database, withId: placeID)
    }
    
    func getAllPlaces() -> [Place]? {
        return self.placesTable.getAllPlaces(fromDB: database)
    }
    
    func deletePlace(placeID:String){
        self.placesTable.deletePlace(fromDB: database, withId: placeID)
    }
    
    func isPlaceInFavorites(placeID:String)->Bool {
       return self.favoritesPlacesTable.getPlace(fromDB: database, withId: placeID) != nil

    }
    
    //favorites places table functions
    func addPlaceToFavorite(place:Place){
        
        self.favoritesPlacesTable.addPlace(toDB: database, place: place)
    }
    
    func getFavoritePlace(placeID:String)->Place? {
        return self.favoritesPlacesTable.getPlace(fromDB: database, withId: placeID)
    }
    
    func getAllFavoritePlaces()->[Place]? {
        return self.favoritesPlacesTable.getAllPlaces(fromDB: database)
    }
    
    
    func removePlaceFromFavorites(placeID:String){
        self.favoritesPlacesTable.deletePlace(fromDB: database, withId: placeID)
    }
    
    func isUserExsit(userId:String)->Bool{
        let ans = self.userTable.isUserExist(romDB: database, userId: userId)
        if ans == true{
            self.userTable.setUserDetails(fromDB: database, withId: userId)
            return true
        }
        else{
            return false
        }
    }
    
}

