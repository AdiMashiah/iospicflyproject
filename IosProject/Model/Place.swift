//
//  Place.swift
//  IosProject
//
//  Created by Dror Manzur on 31/12/2017.
//  Copyright © 2017 Adi Mashiah. All rights reserved.
//

import Foundation
import UIKit

class Place {
    
    var placeId:String?
    var name:String
    var description:String
    var publisher:String?
    var timestamp:Date?
    var imageURL:String?
    var image:UIImage?
    

    init(name:String, description:String,image:UIImage) {
        self.name = name
        self.description = description
        self.image = image

    }
    
    
    init(placeId:String, name:String, description:String,publisher:String) {
        self.placeId = placeId
        self.name = name
        self.description = description
        self.publisher = publisher
    }
    
    
    init(placeId:String, name:String, description:String,publisher:String,imageURL:String,timestamp:Date) {
        self.placeId = placeId
        self.name = name
        self.description = description
        self.publisher = publisher
        self.imageURL = imageURL
        self.timestamp = timestamp;
    }
    
    init(json:Dictionary<String,Any>){
        self.placeId = json["placeId"] as? String
        self.name = json["name"] as! String
        self.description = json["description"] as! String
        self.publisher = json["publisher"] as? String
        if let ts = json["timestamp"] as? Double {
            self.timestamp = Date.fromFirebase(ts)
        }
        if let imageURL = json["imageURL"]{
            self.imageURL = imageURL as? String
        }
    }
    /*
    init(value:NSDictionary) {
        
        self.placeId = value["placeId"] as? String
        self.name =  value["name"] as? String
        self.description =  value["description"] as? String
        self.publisher = value["publisher"] as? String
        if let ts = value["timestamp"] as? Double {
            self.timestamp = Date.fromFirebase(ts)
        }
        if let imageURL =  value["imageURL"]{
            self.imageURL = imageURL as? String
        }
        
   
    }
    */
    class func placeToFirebase(place:Place) -> Dictionary<String,Any> {
        var json = Dictionary<String,Any>()
        json["placeId"] = place.placeId
        json["name"] = place.name
        json["description"] = place.description
        json["publisher"] = place.publisher
        json["imageURL"] = place.imageURL
        return json
    }
    
    
    
}

